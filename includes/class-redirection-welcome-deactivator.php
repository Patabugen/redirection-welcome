<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://patabugen.co.uk
 * @since      1.0.0
 *
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/includes
 * @author     Sami Greenbury <dude@patabugen.co.uk>
 */
class Redirection_Welcome_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		if (Redirection_Welcome_Options::get_option( 'delete_on_deactivate' ) ){
			Redirection_Welcome_Options::delete_option( 'param_name' );
			Redirection_Welcome_Options::delete_option( 'load_magnific' );
			Redirection_Welcome_Options::delete_option( 'number_of_sources' );
			Redirection_Welcome_Options::delete_option( 'delete_on_deactivate' );

			// We'll only actually clean up to 1000, anyone creating more than 1000
			// is probably crazy ;) we've gotta stop somewhere.
			for($i = 1; $i <= 1000; $i++) {
				Redirection_Welcome_Options::delete_option( 'source_'.$i );
				Redirection_Welcome_Options::delete_option( 'content_'.$i );
				Redirection_Welcome_Options::delete_option( 'close_content_on_click_'.$i );
			}

		}
	}

}
