<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://patabugen.co.uk
 * @since      1.0.0
 *
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/includes
 * @author     Sami Greenbury <dude@patabugen.co.uk>
 */
class Redirection_Welcome_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'redirection-welcome',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
