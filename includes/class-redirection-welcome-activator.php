<?php

/**
 * Fired during plugin activation
 *
 * @link       http://patabugen.co.uk
 * @since      1.0.0
 *
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/includes
 * @author     Sami Greenbury <dude@patabugen.co.uk>
 */
class Redirection_Welcome_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		Redirection_Welcome_Option::update_option('param_name', 'source');
		Redirection_Welcome_Option::update_option('load_magnific', 'true');
		Redirection_Welcome_Option::update_option('number_of_sources', 5);
	}

}
