=== Plugin Name ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: http://patabugen.co.uk
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Show popup content to visitors based on the value of a GET variable. For example welcome them based on their referral source

== Description ==
Lets you popup some content when a visitor lands on your page based on the value of a GET variable.

For example if you've moved a site from one domain to another you can add ?source=oldsite to the redirection URL and display a "Welcome to our new domain/branding/website" only to visitors who have come from the old site.

You can add multiple different sources, customise the GET variable name. Uses Magnific as the popup script.

Doesn't add any styles to the popups (except for the Magnific ones), just gives you a rich text box to create content.

== Installation ==

1. Upload to your plugin directory (often `/wp-content/plugins/` )
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Login to wp-admin
1. Click on Redirection Welcome on the left to set the settings/create welcome popups

== Frequently Asked Questions ==

== Screenshots ==

1. Plugin Settings Screen showing what's available
2. Screenshot showing the Welcome Popup screen where you can set a Welcome Popup. The screen repeats for however many you wish to make.
3. Screenshot showing the popup on the front end, with an image as an example.
4. The plugin comes with no styling except the backdrop, so anything other than images will need to be styled to suite.

== Changelog ==

= 1.0 =
* Initial Release

== Upgrade Notice ==
