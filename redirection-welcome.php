<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://patabugen.co.uk
 * @since             1.0.0
 * @package           Redirection_Welcome
 *
 * @wordpress-plugin
 * Plugin Name:       Redirection Welcome
 * Plugin URI:        http://patabugen.co.uk
 * Description:       Shows a popup to a visitor if a particular source/GET param has been given
 * Version:           1.0.0
 * Author:            Sami Greenbury
 * Author URI:        http://patabugen.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       redirection-welcome
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-redirection-welcome-activator.php
 */
function activate_redirection_welcome() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-redirection-welcome-activator.php';
	Redirection_Welcome_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-redirection-welcome-deactivator.php
 */
function deactivate_redirection_welcome() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-redirection-welcome-deactivator.php';
	Redirection_Welcome_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_redirection_welcome' );
register_deactivation_hook( __FILE__, 'deactivate_redirection_welcome' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-redirection-welcome.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_redirection_welcome() {

	$plugin = new Redirection_Welcome();
	$plugin->run();

}
run_redirection_welcome();
