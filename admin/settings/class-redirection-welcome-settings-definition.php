<?php
/**
 *
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/includes
 */

/**
 * The Settings definition of the plugin.
 *
 *
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/includes
 * @author     Your Name <email@example.com>
 */
class Redirection_Welcome_Settings_Definition {

	public static $redirection_welcome = 'redirection-welcome';

	/**
	 * [apply_tab_slug_filters description]
	 *
	 * @param  array $default_settings [description]
	 *
	 * @return array                   [description]
	 */
	static private function apply_tab_slug_filters( $default_settings ) {

		$extended_settings[] = array();
		$extended_tabs       = self::get_tabs();

		foreach ( $extended_tabs as $tab_slug => $tab_desc ) {

			$options = isset( $default_settings[ $tab_slug ] ) ? $default_settings[ $tab_slug ] : array();

			$extended_settings[ $tab_slug ] = apply_filters( 'redirection_welcome_settings_' . $tab_slug, $options );
		}

		return $extended_settings;
	}

	/**
	 * [get_default_tab_slug description]
	 * @return [type] [description]
	 */
	static public function get_default_tab_slug() {

		return key( self::get_tabs() );
	}

	/**
	 * Retrieve settings tabs
	 *
	 * @since    1.0.0
	 * @return    array    $tabs    Settings tabs
	 */
	static public function get_tabs() {

		$tabs                = array();
		$tabs['welcome_popups']  = __( 'Welcome Popups', self::$redirection_welcome );
		$tabs['settings'] = __( 'Settings', self::$redirection_welcome );

		return apply_filters( 'redirection_welcome_settings_tabs', $tabs );
	}

	/**
	 * 'Whitelisted' Redirection_Welcome settings, filters are provided for each settings
	 * section to allow extensions and other plugins to add their own settings
	 *
	 *
	 * @since    1.0.0
	 * @return    mixed    $value    Value saved / $default if key if not exist
	 */
	static public function get_settings() {

		$settings[] = array();
		// DON'T FORGET to reflect changes here in the deactivate method for cleaning up
		// Build the correct number of source options
		$source_options = array();
		for($i = 1; $i <= Redirection_Welcome_Option::get_option('number_of_sources'); $i++) {
				$source_options['source_'.$i] = array(
					'name' => __( 'Source '.$i, self::$redirection_welcome ),
					'desc' => __( 'If the source is equal to this, show Content '.$i.' as a popup', self::$redirection_welcome ),
					'type' => 'text',
				);
				$source_options['close_content_on_click_'.$i] = array(
					'name' => __( 'Close popup on click '.$i, self::$redirection_welcome ),
					'desc' => __( '(Magnific setting) Close popup when user clicks on content of it. It’s recommended to enable this option when you have only image in popup. ', self::$redirection_welcome ),
					'type' => 'checkbox',
				);
				$source_options['content_'.$i] = array(
					'name' => __( 'Content '.$i, self::$redirection_welcome ),
					'desc' => __( 'Content to be displayed if Source is equal to Source '.$i, self::$redirection_welcome ),
					'type' => 'rich_editor',
				);
		}
		$settings = array(
			'welcome_popups' => $source_options,
			'settings' => array(
				'param_name'                       => array(
					'name' => __( 'GET Param Name', self::$redirection_welcome ),
					'desc' => __( 'The name of the Get param we should read to find out our source (e.g. example.com/?<b>source</b>=)', self::$redirection_welcome ),
					'type' => 'text',
				),
				'load_magnific'                   => array(
					'name' => __( 'Include Magnific', self::$redirection_welcome ),
					'desc' => __( 'Should we include the JS and CSS for the <a href="http://dimsemenov.com/plugins/magnific-popup/">Magnific</a>. Uses the handle "magnific".' , self::$redirection_welcome ),
					'type' => 'checkbox',
				),
				'number_of_sources'     => array(
					'name' => __( 'Number of Source options', self::$redirection_welcome ),
					'desc' => __( 'How many sources do you need to be able to define? Setting too many (dozens/hundreds) may clog up your database with empty settings.', self::$redirection_welcome ),
					'min'  => 0,
					'step' => 1,
					'type' => 'number'
				),
				'delete_on_deactivate'     => array(
					'name' => __( 'Delete settings on deactivate', self::$redirection_welcome ),
					'desc' => __( 'Check this if you want to delete all your settings on deactivating, this will tidy up your database but lose any preferences.', self::$redirection_welcome ),
					'type' => 'checkbox'
				),
			),
		);

		return self::apply_tab_slug_filters( $settings );
	}
}
