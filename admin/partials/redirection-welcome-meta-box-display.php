<?php
/**
 * Provide a meta box view for the settings page
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/admin/partials
 */

/**
 * Meta Box
 *
 * Renders a single meta box.
 *
 * @since       1.0.0
*/
?>

<form action="options.php" method="POST">
	<?php settings_fields( 'redirection_welcome_settings' ); ?>
	<?php do_settings_sections( 'redirection_welcome_settings_' . $active_tab ); ?>
	<?php submit_button(); ?>
</form>
<br class="clear" />
