<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://patabugen.co.uk
 * @since      1.0.0
 *
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/public/partials
 */
?>
<div id="welcome-popup-<?=esc_attr($id)?>" class="white-popup mfp-hide">
	<?=wp_kses($html, wp_kses_allowed_html('post'));?>
</div>
<script>
(function($){
	$(document).ready(function() {
		$.magnificPopup.open({
			type:'inline',
			items: {
				src: '#welcome-popup-<?=esc_attr($id)?>'
			},
			closeOnContentClick: <?php echo $close_content_on_click ? 'true' : 'false' ?>
		});
	});
})(jQuery);
</script>
