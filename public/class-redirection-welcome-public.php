<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://patabugen.co.uk
 * @since      1.0.0
 *
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Redirection_Welcome
 * @subpackage Redirection_Welcome/public
 * @author     Sami Greenbury <dude@patabugen.co.uk>
 */
class Redirection_Welcome_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $redirection_welcome    The ID of this plugin.
	 */
	private $redirection_welcome;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The ID of the matching source (e.g. 1, 2, 3 to match up with the settings pag)
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $matching_source_id the ID of the source to display
	 */
	private $matching_source_id;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $redirection_welcome       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $redirection_welcome, $version, $matching_source_id ) {

		$this->redirection_welcome = $redirection_welcome;
		$this->version = $version;
		$this->matching_source_id = $matching_source_id;

	}

	public function display_popup($id)
	{
		$id = $this->matching_source_id;
		$html = Redirection_Welcome_Option::get_option('content_'.$id);

		$close_content_on_click = Redirection_Welcome_Option::get_option('close_content_on_click_'.$id);
		fb($close_content_on_click, 'close_content_on_click');
		require_once(
			plugin_dir_path( dirname( __FILE__ ) )
			. 'public/partials/display-popup.php'
		);
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Redirection_Welcome_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Redirection_Welcome_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		if (Redirection_Welcome_Option::get_option('load_magnific')) {
			wp_enqueue_style(
				'magnific',
				plugin_dir_url( __FILE__ ) . 'css/magnific-popup.css',
				array(),
				'1.1.0',
				'all'
			);
		}

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Redirection_Welcome_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Redirection_Welcome_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		if (Redirection_Welcome_Option::get_option('load_magnific')) {
			wp_enqueue_script( 'magnific',
				plugin_dir_url( __FILE__ ) . 'js/jquery.magnific.min.js',
				array( 'jquery' ),
				'1.1.0',
				false
			);
		}
	}

}
